---
title: Material Purchase Scheduling 
description: Use commodity market data for planned purchasing of materials
author: 
keywords: marp,marp-cli,slide
url: 
marp: true
image: 
---

# Tracking Concept
## Using Bluetooth BLE

---

<style>
img[alt~="center"], .center, .fence {
  display: block;
  margin: 0 auto;
}
kbd {
    background-color: #eee;
    border-radius: 3px;
    border: 1px solid #b4b4b4;
    box-shadow: 0 1px 1px rgba(0, 0, 0, .2), 0 2px 0 0 rgba(255, 255, 255, .7) inset;
    color: #333;
    display: inline-block;
    font-size: .85em;
    font-weight: 700;
    line-height: 1;
    padding: 2px 4px;
    white-space: nowrap;
}
.float-right {
    float: right;
}
.fence img {
    width: auto;
    max-width: 100%;
    height: auto;
    max-height: 100%;
}
.margin-top {
    margin-top: 1em;
}
.padding-top {
    padding-top: 1em;
}
</style>

# Collected stories

- _"I do not know where a person is"_, said :construction_worker: on the :building_construction:
- _"I do not know where a laptop is"_, said :construction_worker: on the :building_construction:
- _"I do not know where something is"_, said :construction_worker: on the :building_construction:

---

# Mindmap

::: fence

@startuml

@startmindmap

*[#Orange] Location information
**[#Orange] Signal presence
*** WiFi coverage
*** Bluetooth coverage
***[#Orange] Bluetooth BLE coverage
****[#Orange] Laptop
****[#Orange] Smartphone
****[#Orange] Battery-powered Beacon
*** LoRa coverage
*** Modulated light (e.g. IR) coverage
*** Ultrasonic coverage
** Signal strength
*** Satellite Triangulation (GPS)
*** ...

@endmindmap

@enduml

:::

---

# Open-source & hardware secondary research

[monitor.sh](https://github.com/andrewjfreyer/monitor/blob/master/monitor.sh): ==A _shell script_ providing passive Bluetooth presence detection of BLE beacons, cell phones, and other Bluetooth devices==

This repository promises a smooth operation on a _Raspberry Pi Zero_

---

# Locked concept

- Attempt to use the (x5) Raspberry Pi Zero devices provided to detect the MAC address of different laptops, smartphones and BLE beacons on the construction site.
- Different Raspberry Pi Zeros are labeled such as "Area 1/2/3"
- Relate the location of the "item" or "person" to the Raspberry Pi Zero station to which the BLE device is closest or have the highest signal strength
- The data can be fed to construction site algorithms (potentially using Machine Learning) to accomplish safety, efficiency, inventory, build progress and staff management functions.
n